<?php

class Film
{
    private $name;
    private $genre;
    private $oscars;

    /**
     * Good constructor.
     * @param $name
     * @param $genre
     * @param $oscars
     */
    public function __construct($name, $genre, $oscars)
    {
        $this->name = $name;
        $this->genre = $genre;
        $this->oscars = $oscars;
    }

    /**
     * @return mixed
     */
    public function getname()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setname($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getgenre()
    {
        return $this->genre;
    }

    /**
     * @param mixed $genre
     */
    public function setgenre($genre)
    {
        $this->genre = $genre;
    }

    /**
     * @return mixed
     */
    public function getOscars()
    {
        return $this->oscars;
    }

    /**
     * @param mixed $oscars
     */
    public function setOscars($oscars)
    {
        $this->oscars = $oscars;
    }


}
