<?php
use PHPUnit\Framework\TestCase;
require_once 'Film.php';
require_once 'Cinema.php';
class Test extends TestCase
{
    private $cinema;
    public function setUp():void
    {
        parent::setUp();
        $films = [];
        $films[] = new Film('a','com',1);
        $films[] = new Film('b','com',2);
        $films[] = new Film('c','com',3);
        $films[] = new Film('d', 'act',4);
        $this->cinema = new Cinema($films);
    }
    public function testOsc()
    {
        $this->assertEquals(4,$this->cinema->countFilms());
    }

}
